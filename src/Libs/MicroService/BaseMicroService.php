<?php

namespace Cy\WWWCityService\Libs\MicroService;

class BaseMicroService
{
    protected $host;
    protected $config_name;

    public function __construct($host, $config_name = '')
    {
        $this->host = $host;
        $this->config_name = $config_name;
        if (!$this->host) abort(5003, '缺少请求host');
    }

    /**
     * 判断值参数是否在数组中，存在则加入参数
     * @param $array 接口可接收的参数
     * @param $data 实际参数
     */
    protected function isParmas($data, $array, $is_empty = false)
    {
        $params = [];
        foreach ($array as $item) {
            if (isset($data[$item]) && (!$is_empty && !empty($value[$item]))) $params[$item] = $data[$item];
        }
        return $params;
    }

    /**
     * 判断值在参数中是否存在
     * @param $value
     * @param $key
     * @return array
     */
    protected function isSet($value, $key)
    {
        $params = [];
        if (is_array($key)) {
            foreach ($key as $item) {
                if (!isset($value[$item]) && empty($value[$item])) abort(422, '【' . $this->config_name . '-微服务】' . $item . '不能为空');
                $params[$item] = $value[$item];
            }
        } else {
            if (!isset($value[$key]) && empty($value[$key])) abort(422, '【' . $this->config_name . '-微服务】' . $key . '不能为空');
        }
        return $params;
    }

    /**
     * 通用的请求方法 - 发送POST请求
     *
     * @param string $url
     * @param array $data
     * @return mixed
     */
    protected function post(string $url, array $data = [])
    {
        $url = '/' . trim($url, '/');

        return AGRequest::getInstance()->post($this->host, $url, $data);
    }

    /**
     * 通用的请求方法 - 发送GET请求
     *
     * @param string $url
     * @param array $data
     * @return mixed
     */
    protected function httpGet(string $url, array $data = [])
    {
        $url = '/' . trim($url, '/');

        return AGRequest::getInstance()->get($this->host, $url, $data);
    }
}
