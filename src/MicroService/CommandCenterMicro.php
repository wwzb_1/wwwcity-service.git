<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;

class CommandCenterMicro extends BaseMicroService
{

    public function hotlineStat($start_date, $end_date, $type='单月', $department=false, $skip=0, $limit=2000)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/hotlineStat/sync',
            [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'type' => $type,
                'department' => $department,
                'skip' => $skip,
                'limit' => $limit
            ]
        );
    }

    public function test(){
        return 'nothing';
    }


    public function hotline($start_date, $end_date, $skip=0, $limit=2000)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/hotline/sync',
            [
                'start_date' => $start_date,
                'end_date' => $end_date,
                'skip' => $skip,
                'limit' => $limit
            ]
        );
    }

    public function economySearch(array $params)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/economy/search',
            $params
        );
    }
}
