<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;

class CouponMicro extends BaseMicroService
{
    //////优惠券模板////////////////////////////////////////////////////////

    /**
     * 创建优惠券模版
     * @param $data
     * @return mixed
     */
    public function coupon_add($data)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/coupons/add',
            $data
        );
    }
    // $data = [
    // 	'logo_url' => '',//票券logo图片
    // 	'title' => '',// -- 必传 -- 模板名称
    // 	'sub_title' => '',//模板副标题
    // 	'notice' => '', //文字提醒
    // 	'description' => '', //卡券描述
    // 	'type_id' => '', // -- 必传 -- 模板类型1：普通券 2：满减券 3：折扣券 4：次卡 5：储值卡
    // 	'satisfy_amount' => '',//满多少消费金额
    // 	'amount' => '', //用券金额
    // 	'discount' => '', //折扣
    // 	'num' => '', //次数
    // 	'limit_type' => '', //使用范围限制类型：为空表示没有使用限制
    // 	'limit_id' => '', //范围限制ID
    // 	'valid_type' => '', //时效 1：一直有效 2：绝对时效（领取后XXX-XXX时间段有效）3：相对时效（领取后N天有效）
    // 	'valid_start_time' => '', //使用开始时间
    // 	'valid_end_time' => '', //使用结束时间
    // 	'valid_days' => '', //自领取之日起有效天数
    // 	'project' => env('Project'), // -- 必传 -- 所属项目标识
    // 	'creator' => '', // -- 必传 -- 创建者id
    // 	'extra' => '' //扩展字段json格式
    // ];

    /**
     * 修改票券模板
     * @param $data
     * @return mixed
     */
    public function coupon_modify($data)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/coupons/modify',
            $data
        );
    }

    /**
     * 获取票券模板列表
     * @param $title
     * @param $sub_title
     * @param $type_id
     * @param $valid_type
     * @param $limit_type
     * @param $limit_id
     * @param $status
     * @param $project
     * @param $creator
     * @param $check_valid
     * @param int $skip
     * @param int $limit
     * @return mixed
     */
    public function coupon_search($title, $sub_title, $type_id, $valid_type, $limit_type, $limit_id, $status, $project, $creator, $check_valid = 0, $skip = 0, $limit = 10)
    {
        $data = [];
        if ($title != null) {
            $data['title'] = $title;
        }
        if ($sub_title != null) {
            $data['sub_title'] = $sub_title;
        }
        if ($type_id != null) {
            $data['type_id'] = $type_id;
        }
        if ($limit_type != null) {
            $data['limit_type'] = $limit_type;
        }
        if ($valid_type != null) {
            $data['valid_type'] = $valid_type;
        }
        if ($limit_id != null) {
            $data['limit_id'] = $limit_id;
        }
        if ($project != null) {
            $data['project'] = $project;
        }
        if ($check_valid != null) {
            $data['check_valid'] = $check_valid;
        }
        if ($creator != null) {
            $data['creator'] = $creator;
        }
        if ($status != null && $status != '') {
            $data['status'] = $status;
        }
        $data['skip'] = $skip;
        $data['limit'] = $limit;

        return AGRequest::getInstance()->post(
            $this->host,
            '/coupons/list',
            $data
        );
    }

    /**
     * 获取票券模板详情
     * @param $id
     * @return mixed
     */
    public function coupon_get($id)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/coupons/get',
            ['id' => $id]
        );
    }

    /**
     * 修改票券模板状态
     * @param $id
     * @param $status // 状态 1：有效 2：无效
     * @return mixed
     */
    public function coupon_modifyStatus($id, $status)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/coupons/modifyStatus',
            [
                'id' => $id,
                'status' => $status
            ]
        );
    }

    //////////////////////////////////////////////////////////////

    /**
     * 获取发放规则列表
     * @param $coupon_id
     * @param $title
     * @param $status
     * @param $channel
     * @param $couponInfo
     * @param $project
     * @param $creator
     * @param int $skip
     * @param int $limit
     * @return mixed
     */
    public function provide_search($coupon_id, $title, $status, $channel, $couponInfo, $project, $creator, $skip = 0, $limit = 10)
    {
        $data = [];
        if ($coupon_id != null) {
            $data['coupon_id'] = $coupon_id;
        }
        if ($title != null) {
            $data['title'] = $title;
        }
        if ($status != null) {
            $data['status'] = $status;
        }
        if ($channel != null) {
            $data['channel'] = $channel;
        }
        if ($couponInfo != null) {
            $data['couponInfo'] = $couponInfo;
        }
        if ($creator != null) {
            $data['creator'] = $creator;
        }
        if ($project != null) {
            $data['project'] = $project;
        }
        $data['skip'] = $skip;
        $data['limit'] = $limit;

        return AGRequest::getInstance()->post(
            $this->host,
            '/provide/list',
            $data
        );
    }

    /**
     * 创建发放规则
     * @param $data
     * @return mixed
     */
    public function provide_add($data)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/provide/add',
            $data
        );
    }

    /**
     * 修改发放规则
     * @param $data
     * @return mixed
     */
    public function provide_modify($data)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/provide/modify',
            $data
        );
    }

    /**
     * 获取发放规则详情
     * @param $id
     * @return mixed
     */
    public function provide_get($id)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/provide/get',
            [
                'id' => $id
            ]
        );
    }

    /**
     * 修改发放规则状态
     * @param $id
     * @param $status
     * @return mixed
     */
    public function provide_modifyStatus($id, $status)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/provide/modifyStatus',
            [
                'id' => $id,
                'status' => $status
            ]
        );
    }

    //////////////////////////////////////////////////////////////

    /**
     * 发放票券
     * @param $provide_id
     * @param $user_id
     * @param int $count
     * @return mixed
     */
    public function entity_create($provide_id, $user_id, $count = 1)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/create',
            [
                'provide_id' => $provide_id,
                'user_id' => $user_id,
                'count' => $count
            ]
        );
    }

    /**
     * 获取票券列表
     * @param string $coupon_id
     * @param string $provide_id
     * @param string $sncode
     * @param string $type_id
     * @param string $limit_type
     * @param string $limit_id
     * @param string $user_id
     * @param string $project
     * @param string $creator
     * @param string $channel
     * @param int $checkStatus
     * @param string $start_time
     * @param string $end_time
     * @param int $skip
     * @param int $limit
     * @return mixed
     * @throws \Exception
     */
    public function entity_search($coupon_id = '', $provide_id = '', $sncode = '', $type_id = '', $limit_type = '', $limit_id = '', $user_id = '', $project = '', $creator = '', $channel = '', $checkStatus = 0, $start_time = '', $end_time = '', $skip = 0, $limit = 100, $status = 1)
    {
        $data = [];
        if (!empty($coupon_id)) {
            $data['coupon_id'] = $coupon_id;
        }
        if (!empty($provide_id)) {
            $data['provide_id'] = $provide_id;
        }
        if (!empty($sncode)) {
            $data['sncode'] = $sncode;
        }
        if (!empty($limit_type)) {
            $data['limit_type'] = $limit_type;
        }
        if (!empty($limit_id)) {
            $data['limit_id'] = $limit_id;
        }
        if (!empty($project)) {
            $data['project'] = $project;
        }
        if (!empty($creator)) {
            $data['coupon_creator'] = $creator;
        }
        if (!empty($type_id)) {
            $data['type_id'] = $type_id;
        }
        if (!empty($user_id)) {
            $data['user_id'] = $user_id;
        }
        if (!empty($channel)) {
            $data['channel'] = $channel;
        }
        if (!empty($checkStatus)) {
            $data['checkStatus'] = $checkStatus;
        }
        if (!empty($start_time)) {
            $data['start_time'] = $start_time;
        }
        if (!empty($end_time)) {
            $data['end_time'] = $end_time;
        }
        if (!empty($status)) {
            $data['status'] = $end_time;
        }

        if (empty($data)) {
            throw new \Exception('请包含一些搜索条件', 2203);
        }

        $data['skip'] = $skip;
        $data['limit'] = $limit;
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/search',
            $data
        );
    }

    /**
     * 获取票券详情
     * @param $id
     * @return mixed
     */
    public function entity_get($id)
    {
        is_int($id) ? $data['id'] = $id : $data['sncode'] = $id;

        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/get',
            $data
        );

    }

    /**
     * 获取票券详情
     * @param $sncode
     * @return mixed
     */
    public function entity_getByCode($sncode)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/get',
            ['sncode' => $sncode]
        );

    }

    public function entity_lock($sncode)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/lock',
            [
                'sncode' => $sncode
            ]
        );
    }

    /**
     * 解锁票券
     * @param $sncode
     * @param $lock_key
     * @return mixed
     */
    public function entity_unLock($sncode, $lock_key)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/unLock',
            [
                'sncode' => $sncode,
                'lock_key' => $lock_key
            ]
        );
    }

    /**
     * 使用票券
     * @param $sncode
     * @param $lock_key
     * @param $money
     * @param $num
     * @param $memo
     * @return mixed
     */
    public function entity_pay($sncode, $lock_key = '', $money = '', $num = '', $memo = '')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/pay',
            [
                'sncode' => $sncode,
                'lock_key' => $lock_key,
                'money' => $money,
                'num' => $num,
                'memo' => $memo
            ]
        );
    }

    /**
     * 回退票券
     * @param $log_id
     * @return mixed
     */
    public function entity_payBack($log_id)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/payBack',
            [
                'log_id' => $log_id
            ]
        );
    }

    public function entity_batch()
    {

    }

    /**
     * 修改票券状态
     * @param $id
     * @param $status
     * @return mixed
     */
    public function entity_modifyStatus($id, $status)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/entity/modifyStatus',
            [
                'id' => $id,
                'status' => $status
            ]
        );
    }
}
