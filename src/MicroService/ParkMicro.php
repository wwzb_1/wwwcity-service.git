<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;

class ParkMicro extends BaseMicroService
{

    public function check($qrcode)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/sentry/check',
            [
                'qrcode' => $qrcode
            ]
        );
    }
}
