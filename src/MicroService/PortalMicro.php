<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;
use Illuminate\Support\Arr;

class PortalMicro extends BaseMicroService
{
    /**
     *
     */
    public function statusers(array $data)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/ys_portal/statusers',
            $data
        );
    }

    /**
     * 为用户设置某个特定的标签
     * @param array $data ['mobile', 'code', 'name', 'type']
     * @return mixed
     */
    public function signByCode(array $data)
    {
        $required = $this->isSet($data, ['mobile', 'code', 'name', 'type', 'method']);

        return $this->post('app/ys_portal/signs', $required);
    }

    /**
     * 根据手机号查询账号
     * @param string $mobile ['mobile']
     * @return mixed
     */
    public function userByMobile(string $mobile)
    {
        return $this->post('app/ys_portal/users/mobile', ['mobile' => $mobile]);
    }
}
