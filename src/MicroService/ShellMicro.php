<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;

class ShellMicro extends BaseMicroService
{
    /**
     * 根据uuid查询居民出入卡
     * @param $uuid
     * @return mixed
     */
    public function doorUserGet($idcard = '', $uuid = '')
    {
        return AGRequest::getInstance()->post($this->host, '/door/user/get', [
            'uuid' => $uuid,
            'idcard' => $idcard,
        ]);
    }

    /**
     * 出入卡用户列表
     * @return mixed
     */
    public function doorUserList($neighborhood, $startTime, $endTime, $page = 0, $size = 20)
    {
        return AGRequest::getInstance()->post($this->host, '/door/user/list', [
            'neighborhood' => $neighborhood,
            'startTime' => $startTime,
            'endTime' => $endTime,
            'page' => $page,
            'size' => $size,
        ]);
    }

    /**
     * 发放积分，如果用户没有账户，自动创建
     * @param $atid
     * @param $point
     * @param $uuid
     * @param string $content
     * @param string $organo
     * @param int $signs
     * @param int $times
     * @param int $points
     * @param int $shares
     * @return mixed
     */
    public function deposit($atid, $point, $uuid, $content='', $organo='', $signs=0, $times=0, $points=0, $shares=0)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/point/deposit',
            [
                'atid' => $atid,
                'point' => $point,
                'uuid' => $uuid,
                'organo' => $organo,
                'signs' => $signs,  // 打卡次数
                'times' => $times,  // 累计时间
                'points' => $points, // 累计积分
                'shares' => $shares, // 分享奖励
                'content' => $content,
            ]
        );
    }

    /**
     * 发放积分，如果用户没有账户，自动创建
     * @param $atid
     * @param $point
     * @param $uuid
     * @param string $destano
     * @param string $content
     * @return mixed
     */
    public function consume($atid, $point, $uuid, $destano='', $content='')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/point/consume',
            [
                'atid' => $atid,
                'point' => $point,
                'uuid' => $uuid,
                'destano' => $destano,
                'content' => $content,
            ]
        );
    }

    /**
     * 以指定的积分兑换指定的物品
     * @param $point
     * @param string $goodsId
     * @param $uuid
     * @param string $memo
     * @return mixed
     */
    public function exchange($point, string $goodsId, $uuid='', $memo='')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/point/exchange',
            [
                'point' => $point,
                'uuid' => $uuid,
                'goods_id' => $goodsId,
                'memo' => $memo,
            ]
        );
    }

    /**
     * 发放积分，如果用户没有账户，自动创建
     * @param string $no
     * @return mixed
     */
    public function notify($no='')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/point/notify',
            [
                'no' => $no
            ]
        );
    }

    public function exchangeGoods($name=false, $status=false, $atid=false, $ids='', $skip=0, $limit=10)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/exchange/search',
            [
                'name' => $name,
                'status' => $status,
                'atid' => $atid,
                'ids' => $ids,
                'skip' => $skip,
                'limit' => $limit
            ]
        );
    }

    public function exchangeLog($name=false, $mobile=false, $atid=false, $status=false, $startTime=0, $endTime=0, $skip=0, $limit=10){
        return AGRequest::getInstance()->post(
            $this->host,
            '/exchange/log',
            [
                'name' => $name,
                'mobile' => $mobile,
                'atid' => $atid,
                'status' => $status,
                'startTime' => $startTime,
                'endTime' => $endTime,
                'skip' => $skip,
                'limit' => $limit
            ]
        );
    }

    /**
     * 核验兑换码
     * @param string $qrcode 兑换码
     * @param string $account 核销员账户
     * @return mixed
     */
    public function exchangeCheck($qrcode,$account){
        return AGRequest::getInstance()->post(
            $this->host,
            '/exchange/check',
            [
                'qrcode'=>$qrcode,
                'account'=>$account
            ]
        );
    }

    /**
     * 核销兑换码
     * @param $qrcode
     * @param $account
     * @param string $ext
     * @return mixed
     */
    public function exchangeConsume($qrcode,$account,$ext='')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/exchange/consume',
            [
                'qrcode'=>$qrcode,
                'account'=>$account,
                'ext'=>$ext,
            ]
        );
    }

    /**
     * 添加兑换商品
     * @param $data
     * @return mixed
     */
    public function exchangeAddGoods($data)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/exchange/addGoods',
            $data
        );
    }

}
