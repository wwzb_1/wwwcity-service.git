<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\AGRequest;
use Cy\WWWCityService\Libs\MicroService\BaseMicroService;
use function GuzzleHttp\Psr7\str;

class UserMicro extends BaseMicroService
{
    //token验证接口
    public function userToken($token)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/token',
            ['token' => $token]
        );
    }

    //登录接口
    public function login($account, $ts, $sign)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/login',
            [
                'account' => $account,
                'ts' => $ts,
                'sign' => $sign,
            ]
        );
    }

    //通过uuid获取用户
    public function getById($uuid)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/getById',
            [
                'id' => $uuid,
            ]
        );
    }

    //通过account获取用户
    public function getByAccount($account)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/getByAccount',
            [
                'account' => $account,
            ]
        );
    }

    //通过account获取用户
    public function getByMobile($mobile)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/getByMobile',
            [
                'mobile' => $mobile,
            ]
        );
    }

    //关键字搜索用户
    public function searchByKey($key)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/searchByKey',
            [
                'key' => $key,
            ]
        );
    }

    //检查用户是否存在
    public function checkRegister($mobile)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/checkRegister',
            [
                'key' => $mobile,
            ]
        );
    }

    // 新增用户
    public function add($data)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/add', $data
        );
    }

    /**
     * 修改用户微服务信息
     * @param  [type] $id       [description]
     * @param  [type] $nickname [description]
     * @param  [type] $address  [description]
     * @param  [type] $idcard   [description]
     * @return [type]           [description]
     */
    public function modify($id, $nickname, $address,$realname='',$mobile='')
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/modify',
            [
                'id' => $id,
                'nickname' => $nickname,
                'address' => $address,
                'realname'=>$realname,
                'mobile'=>$mobile
            ]
        );
    }

    /**
     * 获取详情
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function get($id)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/get',
            [
                'id' => $id,
            ]
        );
    }


    /**
     * 修改密码
     * @param $id
     * @param $password
     * @param $oldpassword
     * @return mixed
     */
    public function modifyPassword($id, $oldpassword, $password)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/user/modifyPassword',
            [
                'id' => $id,
                'password' => $password,
                'oldpassword' => $oldpassword,
            ]
        );
    }

    /**
     * 获取人脸数据
     * @param $id
     * @param $image
     * @return mixed
     */
    public function getFace($id)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/face/get',
            [
                'id' => $id
            ]
        );
    }

    /**
     * 获取身份证照片
     * @param $id
     * @return mixed
     */
    public function getIdCardPhoto($id)
    {

        return AGRequest::getInstance()->post($this->host, '/user/getIDCardImage', [
            'id' => $id
        ]);
    }

    /**
     * 跟据身份证号查询
     * @param $id_card
     * @return mixed
     */
    public function getByIdCard($id_card)
    {
        return AGRequest::getInstance()->post($this->host, '/user/getByIdCard', [
            'idcard' => $id_card
        ]);
    }

    /**
     * 判断用户是否实名认证
     */
    public function isAuth($id)
    {
        return AGRequest::getInstance()->post($this->host, '/user/isAuth', [
            'id' => $id
        ]);
    }

    /**
     * 身份证图片识别
     * @param $image
     * @param $cardside
     * @return mixed
     */
    public function idCardOCR($image, $cardside)
    {
        return AGRequest::getInstance()->post($this->host, '/user/idCardOCR', [
            'image' => $image,
            'cardside' => $cardside
        ]);
    }

    /**
     * 上传身份证图片
     * @param $id
     * @param $image
     * @param string $cardside
     * @param int $isforce
     * @return mixed
     */
    public function uploadIDCard($id, $image, $cardside = 'FRONT', $isforce = 0)
    {
        return AGRequest::getInstance()->post($this->host, '/user/uploadIDCard', [
            'id' => $id,
            'image' => $image,
            'cardside' => $cardside,
            'isforce' => $isforce
        ]);
    }

    /**
     * 通过身份证图片保存用户
     * @param $image
     * @return mixed
     */
    public function saveIDCard($image)
    {
        return AGRequest::getInstance()->post($this->host, '/user/saveIDCard', [
            'image' => $image,
        ]);
    }

    /**
     * 删除用户
     * @param $id
     * @return mixed
     */
    public function del($id)
    {
        return AGRequest::getInstance()->post($this->host, '/user/del', [
            'id' => $id,
        ]);
    }

    /**
     * 修改手机号
     */
    public function modifyMobileForce($id, $mobile)
    {
        return AGRequest::getInstance()->post($this->host, '/user/modifyMobileForce', [
            'id' => $id,
            'mobile' => $mobile,
        ]);
    }

    /**
     * 上传人脸照片
     * @param $id
     * @param $image
     * @param int $isCompareIDCard
     * @return mixed
     */
    public function faceUpload($id, $image, $isCompareIDCard = 1)
    {
        return AGRequest::getInstance()->post($this->host, '/face/upload', [
            'id' => $id,
            'image' => $image,
            'isCompareIDCard' => $isCompareIDCard,
        ]);
    }

    /**
     * 人脸识别
     * @param $id
     * @param $image
     * @return mixed
     */
    public function faceCheck($id, $image)
    {
        return AGRequest::getInstance()->post($this->host, '/face/check', [
            'id' => $id,
            'image' => $image,
        ]);
    }

    /**
     * 车辆驾驶证识别
     * @param $data
     * @return mixed
     */
    public function ocrDrivingLicense($data)
    {
        $this->isSet($data, 'image');
        return AGRequest::getInstance()->post($this->host, '/ocr/driving/license', $data);
    }

    /**
     * 车辆行驶证识别
     * @param $data
     * @return mixed
     */
    public function ocrVehicleLicense($data)
    {
        $this->isSet($data, ['image', 'side']);
        return AGRequest::getInstance()->post($this->host, '/ocr/vehicle/license', $data);
    }

    /**
     * 统计一段时间内，每天的注册数量
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statFace($start, $end)
    {
        return AGRequest::getInstance()->post($this->host, '/stat/face', [
            'start' => $start,
            'end' => $end,
        ]);
    }

    /**
     * 统计一段时间内人脸总数
     * @param $start
     * @param $end
     * @return mixed
     */
    public function statFaceCount($start, $end)
    {
        return AGRequest::getInstance()->post($this->host, '/stat/faceCount', [
            'start' => $start,
            'end' => $end,
        ]);
    }

    // 居民出入卡列表 card/list
    public function cardList($ic_type = 0, $uuid = '', $status = 0, $cardNo = '', $mobile = '', $identity = '', $ic = '',
                             $name = '', $issuer = '', $skip = 0, $limit = 10)
    {
        $data = [
            'ic_type' => $ic_type,
            'uuid' => $uuid,
            'status' => $status,
            'card_no' => $cardNo,
            'mobile' => $mobile,
            'identity' => $identity,
            'ic' => $ic,
            'name' => $name,
            'issuer' => $issuer,
            'skip' => $skip,
            'limit' => $limit
        ];
        return AGRequest::getInstance()->post($this->host, '/card/list', $data);
    }


    // 添加出入卡 card/add
    public function addCard($uuid, $type, $street_code = '', $community_code = '', $issuer = '', $ic_no = '')
    {
        if (empty($issuer)) {
            $issuer = '未知单位';
        }
        $data = [
            'uuid' => $uuid,
            'type' => $type,
            'street_code' => $street_code,
            'community_code' => $community_code,
            'issuer' => $issuer,
            'ic_no' => $ic_no
        ];
        return AGRequest::getInstance()->post($this->host, '/card/add', $data);
    }

    // 出入卡详情 card/detail
    public function cardDetail($uuid, $type = 0, $with_agree = 0)
    {
        $data = [
            'uuid' => $uuid,
            'ic_type' => $type,
            'with_agree' => $with_agree
        ];
        return AGRequest::getInstance()->post($this->host, '/card/detail', $data);
    }

    public function getCardByNo($no = '')
    {
        $data = [
            'no' => $no
        ];
        return AGRequest::getInstance()->post($this->host, '/card/getByNo', $data);
    }

    public function getCard($id = 0, $ic_type = 0, $uuid = '')
    {
        $data = [
            'id' => $id,
            'uuid' => $uuid,
            'ic_type' => $ic_type
        ];
        return AGRequest::getInstance()->post($this->host, '/card/get', $data);
    }

    // 修改出入卡 card/modify
    public function modifyCard($id, $status = 0, $ic_no = 0, $type = '', $street_code='', $community_code='', $issuer='')
    {
        $data = [
            'id' => $id,
            'status' => $status,
            'ic_no' => $ic_no,
            'type' => $type,
            'street_code' => $street_code,
            'community_code' => $community_code,
            'issuer' => $issuer
        ];
        return AGRequest::getInstance()->post($this->host, '/card/modify', $data);
    }

    // 验证出入卡二维码 card/check/qrcode
    public function checkCardQrcode($qrcode)
    {
        $data = [
            'qrcode' => $qrcode
        ];
        return AGRequest::getInstance()->post($this->host, '/card/check/qrcode', $data);
    }

    // 刷新出入卡图片
    public function refreshCard($id)
    {
        $data = [
            'id' => $id
        ];
        return AGRequest::getInstance()->post($this->host, '/card/reset', $data);
    }


    // 出入卡修改记录 card/changes
    public function cardChanges($type = 0, $startTime = 0, $endTime = 0, $skip = 0, $limit = 10)
    {
        $data = [
            'type' => $type,
            'startTime' => $startTime,
            'endTime' => $endTime,
            'skip' => $skip,
            'limit' => $limit
        ];
        return AGRequest::getInstance()->post($this->host, '/card/changes', $data);
    }


    // 车辆品牌 car/brand
    public function carBrands($short = '')
    {
        $data = [
            'short' => $short
        ];
        return AGRequest::getInstance()->post($this->host, '/car/brand', $data);
    }

    // 车辆类型 car/types
    public function carTypes()
    {
        $data = [];
        return AGRequest::getInstance()->post($this->host, '/car/types', $data);
    }

    // 车辆列表 car/list
    public function cars($uuid = '', $name = '', $cardNo = '', $icNo = '', $mobile = '', $identity = '', $car_no = '',
                         $typeId = 0, $brandId = 0, $skip = 0, $limit = 10)
    {
        $data = [
            'uuid' => $uuid,
            'name' => $name,
            'card_no' => $cardNo,
            'ic_no' => $icNo,
            'mobile' => $mobile,
            'identity' => $identity,
            'car_no' => $car_no,
            'type_id' => $typeId,
            'brand_id' => $brandId,
            'skip' => $skip,
            'limit' => $limit
        ];
        return AGRequest::getInstance()->post($this->host, '/car/list', $data);
    }

    // 增加车辆 car/add
    public function addCar($uuid, $car_no, $typeId, $brandId, $reset = false, $no_color = '', $car_color = '')
    {
        $data = [
            'uuid' => $uuid,
            'car_no' => $car_no,
            'type_id' => $typeId,
            'brand_id' => $brandId,
            'reset' => $reset,
            'no_color' => $no_color,
            'car_color' => $car_color
        ];
        return AGRequest::getInstance()->post($this->host, '/car/add', $data);
    }

    // 修改车辆 car/modify
    public function modifyCar($id, $car_no = '', $status = 0, $typeId = 0, $brandId = 0, $no_color = '', $car_color = '')
    {
        $data = [
            'id' => $id,
            'car_no' => $car_no,
            'status' => $status,
            'type_id' => $typeId,
            'brand_id' => $brandId,
            'no_color' => $no_color,
            'car_color' => $car_color
        ];
        return AGRequest::getInstance()->post($this->host, '/car/modify', $data);
    }

     // 居民出入卡地址列表 card/address/list
    public function cardAddresses($uuid = '', $card_id = 0, $room_id = 0, $name = '', $cardNo = '', $mobile = '', $identity = '',
                                  $icNo = '', $isDefault = 0, $street_code = '', $community_code = '', $building_code = '',
                                  $unit_code = '', $skip = 0,
                                  $limit = 10)
    {
        $data = [
            'uuid' => $uuid,
            'card_id' => $card_id,
            'room_id' => $room_id,
            'name' => $name,
            'card_no' => $cardNo,
            'mobile' => $mobile,
            'identity' => $identity,
            'ic_no' => $icNo,
            'is_default' => $isDefault,
            'street_code' => $street_code,
            'community_code' => $community_code,
            'building_code' => $building_code,
            'unit_code' => $unit_code,
            'skip' => $skip,
            'limit' => $limit
        ];
        return AGRequest::getInstance()->post($this->host, '/card/address/list', $data);
    }


    // 添加地址 card/address/add
    public function addCardAddress($card_id, $type, $isDefault = 0, $street_code = '', $community_code = '',
                                   $building_code='',
                                   $unit_code = '', $room_id = 0, $room_no = '', $neighborhoodId = 0, $address = '',
                                   $startTime = 0,
                                   $endTime = 0)
    {
        $data = [
            'card_id' => $card_id,
            'type' => $type,
            'is_default' => $isDefault,
            'street_code' => $street_code,
            'community_code' => $community_code,
            'building_code' => $building_code,
            'unit_code' => $unit_code,
            'room_id' => $room_id,
            'room_no' => $room_no,
            'neighborhood_id' => $neighborhoodId,
            'address' => $address,
            'start_time' => $startTime,
            'end_time' => $endTime
        ];
        return AGRequest::getInstance()->post($this->host, '/card/address/add', $data);
    }


    // 修改地址 card/address/modify
    public function modifyCardAddress($id, $status = 0, $type = '', $isDefault = -1, $street_code = -1,
                                      $community_code = -1, $building_code = -1,
                                      $unit_code = -1, $room_id = -1, $room_no = '', $neighborhoodId = -1, $address =
                                      '', $startTime = 0,
                                      $endTime = 0)
    {
        $data = [
            'id' => $id,
            'status' => $status,
            'type' => $type,
            'is_default' => $isDefault,
            'street_code' => $street_code,
            'community_code' => $community_code,
            'unit_code' => $unit_code,
            'room_id' => $room_id,
            'room_no' => $room_no,
            'address' => $address,
            'start_time' => $startTime,
            'end_time' => $endTime
        ];
        return AGRequest::getInstance()->post($this->host, '/card/address/modify', $data);
    }


    // 地址信息 card/address/detail
    public function cardAddressDetail($id)
    {
        $data = [
            'id' => $id
        ];
        return AGRequest::getInstance()->post($this->host, '/card/address/detail', $data);
    }


    // 车辆详情
    public function carDetail($id)
    {
        $data = [
            'id' => $id
        ];
        return AGRequest::getInstance()->post($this->host, '/car/get', $data);
    }

    // 同意用户协议
    public function userAgree($uuid, $types = '', $from = '')
    {
        $data = [
            'uuid' => $uuid,
            'types' => $types,
            'from' => $from
        ];
        return AGRequest::getInstance()->post($this->host, '/user/agree', $data);
    }

    // 根据同意的用户协议类型获取数据
    public function getByUserAgree($uuid, $type)
    {
        $data = [
            'id' => $uuid,
            'type' => $type
        ];
        return AGRequest::getInstance()->post($this->host, '/user/getby/agree', $data);
    }

    // 获取用户和出入卡信息
    public function getUserAndCard($uuid, $fuzzy_idcard=0)
    {
        $data = [
            'uuid' => $uuid,
            'fuzzy_idcard' => $fuzzy_idcard
        ];
        return AGRequest::getInstance()->post($this->host, '/card/getUserAndCard', $data);
    }

    /**
     * 出入卡数据统计
     * 可根据group同时获取多类统计数据，group的值为total,age,gender,type,street,community的组合字符串，用逗号,隔开
     * group的值含义total:出入卡总数,age:按年龄统计,gender:按性别统计,type:按身份类型统计,street:按街道统计数量,community:按社区统计数量
     */
    public function statCard($group='',$street_code='',$community_code='')
    {
        $data = [
            'group' => $group,
            'street_code' => $street_code,
            'community_code' => $community_code
        ];
        return AGRequest::getInstance()->post($this->host, '/stat/card', $data);
    }

    /**
     * 出入卡解绑
     * @param $uuid
     * @return mixed
     */
    public function cardUnbind($uuid)
    {
        $data = [
            'uuid' => $uuid
        ];
        return AGRequest::getInstance()->post($this->host, '/card/unbing', $data);
    }


    //通过account获取用户
    public function getCardByIc($type, $ic)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/card/getByIc',
            [
                'type' => $type,
                'ic' => $ic,
            ]
        );
    }

    // 创建临时出入卡
    public function createTmpCard($idcard, $name, $mobile)
    {
        return AGRequest::getInstance()->post(
            $this->host,
            '/card/getTmpCard',
            [
                'idcard' => $idcard,
                'name' => $name,
                'mobile' => $mobile,
            ]
        );
    }
}
