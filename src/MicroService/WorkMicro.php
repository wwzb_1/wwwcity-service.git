<?php

namespace Cy\WWWCityService\MicroService;

use Cy\WWWCityService\Libs\MicroService\BaseMicroService;
use Illuminate\Support\Arr;

class WorkMicro extends BaseMicroService
{
    /*
    |--------------------------------------------------------------------------
    | 消息
    |--------------------------------------------------------------------------
    */
    /**
     * 新增消息
     *
     * @param array $data ['app_id', 'title', 'content', 'client_uuid', 'category_id']
     */
    public function messageStore(array $data)
    {
        $data['app_id'] = env('APP_ID');

        # 必填参数
        $required = $this->isSet($data, ['app_id', 'title', 'content', 'client_uuid', 'category_id']);

        # 存在且不为空时传入的参数
        $other = $this->isParmas($data, ['client_nickname', 'client_avatar', 'from_client_uuid', 'from_client_nickname', 'from_client_avatar', 'cover', 'relation_id', 'relation_extension']);

        # 合并参数
        $data = array_merge($required, $other);

        return $this->post('api/admin/message/store', $data);
    }

    /**
     * 获取某人的未读消息数量
     */
    public function unreadCount(array $data)
    {
        $data['app_id'] = env('APP_ID');
        $required = $this->isSet($data, ['app_id', 'client_uuid']);
        $other = $this->isParmas($data, ['category_id', 'category_ids']);

        return $this->httpGet('api/admin/message/unread', array_merge($required, $other));
    }

    /**
     * 清除未读消息
     */
    public function readAll(array $data)
    {
        $data['app_id'] = env('APP_ID');
        $required = $this->isSet($data, ['app_id', 'client_uuid']);
        $other = $this->isParmas($data, ['category_ids', 'category_tags']);

        return $this->post('api/admin/message/read', array_merge($required, $other));
    }
}
