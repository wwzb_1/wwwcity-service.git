<?php

namespace Cy\WWWCityService;

use Cy\WWWCityService\Libs\MicroService\BaseMicroService;

class WWWCityService
{
    private $config;

    public function __construct()
    {
        $this->config = config('wwwcityservice');
    }

    /**
     * @param string $config_name
     * @return BaseMicroService
     */
    public function service(string $config_name)
    {
        $host = $this->config[$config_name]['HOST'];

        if($config_name == 'ag'){
            $class = 'Cy\WWWCityService\MicroService\AgMicro';
        } else {
            $class = 'Cy\WWWCityService\MicroService\\' . ucwords($config_name) . 'Micro';
        }

        if (class_exists($class)) {
            return new $class($host, $config_name);
        }

        abort(5000, '微服务' . $config_name . '未开发');
    }

}
